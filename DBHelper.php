<?php

require_once 'Helper.php';

class DBHelper
{
    private static $sql = null;

    public static function getConnection()
    {
        if (DBHelper::$sql == null) {
            DBHelper::$sql = mysqli_connect(Config::$sqlHostname, Config::$sqlUsername, Config::$sqlPassword, Config::$sqlDatabase);

            if (mysqli_connect_errno() != 0 || !DBHelper::$sql->set_charset('utf8')) {
                outputError(11);
            }
            return DBHelper::$sql;
        } else {
            return DBHelper::$sql;
        }
    }
}