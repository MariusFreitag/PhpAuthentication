<?php

header("Content-Type: text/plain");
require_once 'Helper.php';

$data = getMode();

switch ($data['mode']) {
    case 'checkAuthToken':
        $account = Account::getByEmail($data['email']);
        if ($account == false) {
            outputError(4);
        }
        Sessions::tryContinue($account, $data['authToken'], $data['fingerprint']);
        break;

    case 'editEmail':
        $account = Account::getByEmail($data['email']);
        if ($account == false) {
            outputError(4);
        }
        if ($data['password'] != $account->getPassword()) {
            outputError(7);
        }
        if (!isEmailAddress($data['newEmail'])) {
            outputError(2);
        }
        if (!$account->setEmail($data['newEmail'])) {
            outputError(99);
        }
        Sessions::close($account);
        break;

    case 'editPassword':
        $account = Account::getByEmail($data['email']);
        if ($account == false) {
            outputError(4);
        }
        if ($data['password'] != $account->getPassword()) {
            outputError(7);
        }
        if (strlen($data['newPassword']) < 6) {
            outputError(6);
        }
        if (!$account->setPassword($data['newPassword'])) {
            outputError(99);
        }
        Sessions::close($account);
        break;

    case 'enable':
        $account = Account::getByEmail($data['email']);
        if ($account == false) {
            outputError(4);
        }
        if (!$account->setIsActive(true)) {
            outputError(99);
        }
        break;

    case 'disable':
        $account = Account::getByEmail($data['email']);
        if ($account == false) {
            outputError(4);
        }
        if (!$account->setIsActive(false)) {
            outputError(99);
        }
        Sessions::close($account);
        break;

    case 'getInfo':
        $account = Account::getByEmail($data['email']);
        if ($account == false) {
            $additionalInformation["exists"] = false;
            $additionalInformation["creationDate"] = '';
            $additionalInformation["isActive"] = '';
            $additionalInformation["isAdmin"] = '';
            $additionalInformation["loggedIn"] = '';
            $additionalInformation["lastLogin"] = '';
            outputAdditionalInformation($additionalInformation, "account information is included");
        } else {
            $account->outputInformation();
        }
        break;

    case 'login':
        $account = Account::getByEmail($data['email']);
        if ($account == false) {
            outputError(4);
        }
        if ($data['password'] != $account->getPassword()) {
            outputError(7);
        }
        Sessions::open($account, $data['fingerprint']);
        break;

    case 'logout':
        $account = Account::getByEmail($data['email']);
        if ($account == false) {
            outputError(4);
        }
        Sessions::close($account);
        break;

    case 'register':
        Account::create($data['email'], $data['password']);
        break;

    case 'resetPassword':
        $account = Account::getByEmail($data['email']);
        if ($account->getPasswordResetToken() != $data['resetToken']) {
            outputError(10);
        }
        if (strlen($data['newPassword']) < 6) {
            outputError(6);
        }
        if (!$account->setPassword($data['newPassword'])) {
            outputError(99);
        }
        if (!$account->setPasswordResetToken('n/a')) {
            outputError(99);
        }
        Sessions::close($account);
        break;

    case 'sendPasswordResetToken':
        $account = Account::getByEmail($data['email']);
        if ($account == false) {
            outputError(4);
        }
        $account->sendPasswordResetToken();
        break;

    default:
        outputError(99);
}

outputSuccess();

function getMode()
{
    $result = Array();

    if (!isset($_GET['mode'])) {
        outputError(1);
    }

    if (!isset($_GET['email'])) {
        outputError(1);
    }
    $result['email'] = strtolower($_GET['email']);

    switch (strtolower($_GET['mode'])) {
        case 'checkauthtoken':
            if (!isset($_GET['authToken']) || !isset($_GET['fingerprint'])) {
                outputError(1);
            }
            $result['mode'] = 'checkAuthToken';
            $result['fingerprint'] = $_GET['fingerprint'];
            $result['authToken'] = $_GET['authToken'];
            return $result;

        case 'editemail':
            if (!isset($_GET['newEmail']) || !isset($_GET['password'])) {
                outputError(1);
            }
            $result['mode'] = 'editEmail';
            $result['password'] = $_GET['password'];
            $result['newEmail'] = $_GET['newEmail'];
            return $result;

        case 'editpassword':
            if (!isset($_GET['newPassword']) || !isset($_GET['password'])) {
                outputError(1);
            }
            $result['mode'] = 'editPassword';
            $result['password'] = $_GET['password'];
            $result['newPassword'] = $_GET['newPassword'];
            return $result;

        case 'enable':
            $result['mode'] = 'enable';
            return $result;

        case 'disable':
            $result['mode'] = 'disable';
            return $result;

        case 'getinfo':
            $result['mode'] = 'getInfo';
            return $result;

        case 'login':
            if (!isset($_GET['password']) || !isset($_GET['fingerprint'])) {
                outputError(1);
            }
            $result['mode'] = 'login';
            $result['password'] = $_GET['password'];
            $result['fingerprint'] = $_GET['fingerprint'];
            return $result;

        case 'logout':
            $result['mode'] = 'logout';
            return $result;

        case 'register':
            if (!isset($_GET['password'])) {
                outputError(1);
            }
            $result['mode'] = 'register';
            $result['password'] = $_GET['password'];
            return $result;

        case 'resetpassword':
            if (!isset($_GET['newPassword']) || !isset($_GET['resetToken'])) {
                outputError(1);
            }
            $result['mode'] = 'resetPassword';
            $result['resetToken'] = $_GET['resetToken'];
            $result['newPassword'] = $_GET['newPassword'];
            return $result;

        case 'sendpasswordresettoken':
            $result['mode'] = 'sendPasswordResetToken';
            return $result;
        default:
            outputError(1);
            return null;
    }
}
