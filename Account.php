<?php

require_once 'Helper.php';

class Account
{

    private $id;
    private $email;
    private $password;
    private $creationDate;
    private $isActive;
    private $isAdmin;
    private $authToken;
    private $lastLogin;
    private $passwordResetToken;

    public static function create($email, $password)
    {
        if (!isEmailAddress($email)) {
            outputError(2);
        }

        if (Account::getByEmail($email) != false) {
            outputError(3);
        }

        if (strlen($password) < 6) {
            outputError(6);
        }

        $query = "INSERT INTO `accounts`(`email`, `password`, `creationDate`, `isActive`, `isAdmin`, `authToken`, `lastLogin`, `passwordResetToken`)
                                  VALUES ('%s',       '%s',       '%s',         '%s',       '%s',       '%s',       '%s',           '%s')";
        $formattedQuery = sprintf($query, strtolower($email), $password, time(), "1", "0", "n/a", "n/a", "n/a", "n/a");

        if (mysqli_query(DBHelper::getConnection(), $formattedQuery) != false) {
            outputSuccess();
        } else {
            outputError(99);
        }
    }

    public static function getByEmail($email)
    {
        $result = new Account();
        $rawQuery = mysqli_query(DBHelper::getConnection(), "SELECT * FROM `accounts` WHERE `email` = '" . strtolower($email) . "'");

        if ($rawQuery == false || mysqli_num_rows($rawQuery) == 0) {
            return false;
        }

        $query = mysqli_fetch_array($rawQuery);

        $result->id = $query['id'];
        $result->email = $query['email'];
        $result->password = $query['password'];
        $result->creationDate = $query['creationDate'];
        $result->isActive = $query['isActive'];
        $result->isAdmin = $query['isAdmin'];
        $result->authToken = $query['authToken'];
        $result->lastLogin = $query['lastLogin'];
        $result->passwordResetToken = $query['passwordResetToken'];

        return $result;
    }

    public static function getByID($id)
    {
        $result = new Account();
        $rawQuery = mysqli_query(DBHelper::getConnection(), "SELECT * FROM `accounts` WHERE `id` = '" . $id . "'");

        if ($rawQuery == false || mysqli_num_rows($rawQuery) == 0) {
            return false;
        }

        $query = mysqli_fetch_array($rawQuery);

        $result->id = $query['id'];
        $result->email = $query['email'];
        $result->password = $query['password'];
        $result->creationDate = $query['creationDate'];
        $result->isActive = $query['isActive'];
        $result->isAdmin = $query['isAdmin'];
        $result->authToken = $query['authToken'];
        $result->lastLogin = $query['lastLogin'];
        $result->passwordResetToken = $query['passwordResetToken'];

        return $result;
    }

    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;
        return mysqli_query(DBHelper::getConnection(), "UPDATE `accounts` SET `authToken` = '" . $authToken . "' WHERE `email` = '" . $this->email . "'") != false;
    }

    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;
        return mysqli_query(DBHelper::getConnection(), "UPDATE `accounts` SET `lastLogin` = '" . $lastLogin . "' WHERE `email` = '" . $this->email . "'") != false;
    }

    public function setEmail($email)
    {
        $oldEmail = $this->email;
        $this->email = strtolower($email);
        return mysqli_query(DBHelper::getConnection(), "UPDATE `accounts` SET `email` = '" . $this->email . "' WHERE `email` = '" . $oldEmail . "'") != false;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return mysqli_query(DBHelper::getConnection(), "UPDATE `accounts` SET `password` = '" . $password . "' WHERE `email` = '" . $this->email . "'") != false;
    }

    public function setPasswordResetToken($passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;
        return mysqli_query(DBHelper::getConnection(), "UPDATE `accounts` SET `passwordResetToken` = '" . $passwordResetToken . "' WHERE `email` = '" . $this->email . "'") != false;
    }

    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return mysqli_query(DBHelper::getConnection(), "UPDATE `accounts` SET `isActive` = '" . $isActive . "' WHERE `email` = '" . $this->email . "'") != false;
    }

    public function outputInformation()
    {
        $additionalInformation["exists"] = true;
        $additionalInformation["creationDate"] = $this->creationDate;
        $additionalInformation["isActive"] = ($this->isActive == 1 ? true : false);
        $additionalInformation["isAdmin"] = ($this->isAdmin == 1 ? true : false);
        $additionalInformation["loggedIn"] = ($this->authToken != "n/a" ? true : false);
        $additionalInformation["lastLogin"] = $this->lastLogin;

        outputAdditionalInformation($additionalInformation, "account information is included");
    }

    public function sendPasswordResetToken()
    {
        $resetToken = substr(generateProductKey(), 0, 5);

        $message = "Your password reset token is: <strong>" . $resetToken . "</strong> <br />";
        $errorMessage = "The generation of your password reset token failed. Please contact the administrator or try again.";

        $success = $this->setPasswordResetToken($resetToken);

        sendMail($this->email, "noreply@mariusfreitag.com", "Password Recovery", $success ? $message : $errorMessage);
    }

    public function resetPassword($passwordResetToken, $newPassword)
    {
        if ($passwordResetToken != $this->passwordResetToken) {
            outputError(10);
        }

        if ($newPassword < 6) {
            outputError(6);
        }

        if (!$this->setPassword($newPassword)) {
            outputError(99);
        } else {
            outputSuccess();
        }
    }

    public function editEmail($password, $newEmail)
    {
        if ($password != $this->password) {
            outputError(7);
        }

        if (!isEmailAddress($newEmail)) {
            outputError(2);
        }

        if (!$this->setEmail($newEmail)) {
            outputError(99);
        } else {
            outputSuccess();
        }
    }

    public function editPassword($password, $newPassword)
    {
        if ($password != $this->password) {
            outputError(7);
        }

        if ($newPassword < 6) {
            outputError(6);
        }

        if (!$this->setPassword($newPassword)) {
            outputError(99);
        } else {
            outputSuccess();
        }
    }

    #region Default Getters
    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function isActive()
    {
        return $this->isActive;
    }

    public function isAdmin()
    {
        return $this->isAdmin;
    }

    public function getAuthToken()
    {
        return $this->authToken;
    }

    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }
    #endregion
}
