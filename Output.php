<?php

require_once 'Helper.php';

/*
 * Response-Style:
 * {
 *  success : bool
 *  errorCode : int
 *  comment : string
 *  additionalInformation : { }
 * }
 */

/*
 * Error-Codes:
 * -1 : additionalInformation included
 * 0 : success
 * 1 : wrong parameters
 * 2 : not an email address
 * 3 : account already exists
 * 4 : account does not exist
 * 5 : invalid authToken
 * 6 : short password (at least 6 characters)
 * 7 : invalid password
 * 8 : invalid registrationToken
 * 9 : invalid jsonString
 * 10 : invalid resetToken
 * 11 : could not connect to the database
 * 12 : the database has a wrong structure
 * 99 : unknown error
 * 
 */

function errors()
{
    return Array(
        -1 => 'additionalInformation included',
        0 => '',
        1 => 'wrong parameters',
        2 => 'not an email address',
        3 => 'account already exists',
        4 => 'account does not exist',
        5 => 'invalid authToken',
        6 => 'short password(at least 6 characters)',
        7 => 'invalid password',
        8 => 'invalid registrationToken',
        9 => 'invalid jsonString',
        10 => 'invalid resetToken',
        11 => 'could not connect to the database',
        12 => 'the database has a wrong structure',
        99 => 'unknown error'
    );
}

function outputError($errorCode)
{
    if (array_key_exists($errorCode, errors()))
        output($errorCode, errors()[$errorCode]);
    else
        output($errorCode, errors()[99]);
}

function outputSuccess()
{
    outputError(0);
}

function output($errorCode, $comment, $additionalInformation = null)
{
    if ($additionalInformation == null) {
        $additionalInformation = Array('' => '');
    }

    $response["success"] = $errorCode == 0;
    $response["errorCode"] = $errorCode;
    $response["comment"] = $comment;
    $response["additionalInformation"] = $additionalInformation;

    exit(json_encode($response, JSON_PRETTY_PRINT));
}

function outputAdditionalInformation($additionalInformation, $comment)
{
    output(0, $comment, $additionalInformation);
}
