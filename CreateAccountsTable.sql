CREATE TABLE IF NOT EXISTS accounts (
  id                 INT(10)       NOT NULL AUTO_INCREMENT PRIMARY KEY,
  email              VARCHAR(255) NOT NULL UNIQUE,
  password           VARCHAR(255) NOT NULL,
  creationDate       VARCHAR(255) NOT NULL,
  isActive           TINYINT(1)    NOT NULL DEFAULT '0',
  isAdmin            TINYINT(1)    NOT NULL DEFAULT '0',
  authToken          VARCHAR(255) NOT NULL DEFAULT 'n/a',
  lastLogin          VARCHAR(255) NOT NULL DEFAULT 'n/a',
  passwordResetToken VARCHAR(255) NOT NULL DEFAULT 'n/a'
)
  ENGINE = MyISAM
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1
  COLLATE = latin1_german1_ci;