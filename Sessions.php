<?php

require_once 'Helper.php';

class Sessions
{

    public static function close($account)
    {
        if ($account->setAuthToken("n/a")) {
            outputSuccess();
        } else {
            outputError(99);
        }
    }

    public static function open($account, $deviceFingerprint)
    {
        $rawToken = generateToken();
        $authToken = md5($rawToken . $deviceFingerprint);

        if ($account->setLastLogin(time()) == true && $account->setAuthToken($authToken) == true) {
            $additionalInformation['authToken'] = $rawToken;
            outputAdditionalInformation($additionalInformation, '"authToken" is included as additionalInformation');
        } else {
            outputError(99);
        }
    }

    public static function tryContinue($account, $rawToken, $deviceFingerprint)
    {
        $authToken = md5($rawToken . $deviceFingerprint);

        if ($account->getAuthToken() == $authToken) {
            outputSuccess();
        } else {
            outputError(5);
        }
    }

}
