<?php

class SsoClient
{
    private static $apiPath = "https://accounts.mariusfreitag.de";
    private static $paramPassphrase = "just_for_obfuscation";
    private static $encryptionMethod = "AES-128-ECB";

    private static $isAdmin = false;

    public static function initialize()
    {
        if (isset($_GET["sso"])) {
            $ssoParams = preg_split('/&/', openssl_decrypt($_GET["sso"], SsoClient::$encryptionMethod, SsoClient::$paramPassphrase));

            if (sizeof($ssoParams) != 2) {
                $ssoParams = Array('', '');
            }

            $_SESSION["accountEmail"] = $ssoParams[0];
            $_SESSION["authToken"] = $ssoParams[1];


            SsoClient::checkAuthToken();

            header('Location: /');
            exit();
        } else {
            SsoClient::checkAuthToken();
        }
    }

    public static function checkAuthToken()
    {
        if (!isset($_SESSION["authToken"]) || !isset($_SESSION["accountEmail"]) || $_SESSION["authToken"] == "" || $_SESSION["accountEmail"] == "") {
            return false;
        }

        $data = json_decode(SsoClient::getWebsiteData(SsoClient::$apiPath . "?mode=checkAuthToken&email=" . $_SESSION["accountEmail"] . "&authToken=" . $_SESSION["authToken"] . "&fingerprint="), true);

        $loggedIn = $data["success"];

        if (!$loggedIn) {
            $_SESSION["authToken"] = "";
            $_SESSION["accountEmail"] = "";
        } else {
            SsoClient::getAccountInfo();
        }

        return $loggedIn;
    }

    public static function login()
    {
        header('Location: ' . SsoClient::$apiPath . '/sso' . SsoClient::generateRedirectParam());
        exit();
    }

    public static function logout()
    {
        header('Location: ' . SsoClient::$apiPath . '/sso' . SsoClient::generateRedirectParam() . '&logout');
        exit();
    }

    public static function isAdmin()
    {
        return SsoClient::$isAdmin;
    }

    private static function getAccountInfo()
    {
        $accountInfo = json_decode(SsoClient::getWebsiteData(SsoClient::$apiPath . "?mode=getinfo&email=" . $_SESSION["accountEmail"]), true);
        SsoClient::$isAdmin = (isset($accountInfo['additionalInformation']['isAdmin']) && $accountInfo['additionalInformation']['isAdmin'] == true);
    }

    private static function getWebsiteData($url)
    {
        $context = stream_context_create(array(
            'http' => array(
                'header' => 'Authorization: Basic ' . base64_encode("login:password")
            )
        ));
        return file_get_contents($url, false, $context);
    }

    private static function generateRedirectParam()
    {
        return "?r=" . urlencode(openssl_encrypt('//' . $_SERVER['HTTP_HOST'], SsoClient::$encryptionMethod, SsoClient::$paramPassphrase));
    }
}