<?php

class SsoServer
{
    private static $apiPath = "https://accounts.mariusfreitag.de";
    private static $paramPassphrase = "just_for_obfuscation";
    private static $encryptionMethod = "AES-128-ECB";

    // For faster loading through caching
    private static $authTokenValidity = null;

    public static function generateSecureParams()
    {
        if (!isset($_SESSION['accountEmail']))
            $_SESSION['accountEmail'] = 'empty';

        if (!isset($_SESSION['authToken']))
            $_SESSION['authToken'] = 'empty';

        return '?sso=' . urlencode(
                openssl_encrypt(
                    $_SESSION['accountEmail'] . '&' . $_SESSION['authToken'],
                    SsoServer::$encryptionMethod,
                    SsoServer::$paramPassphrase));
    }

    public static function redirectToOrigin()
    {
        header('Location: ' . openssl_decrypt($_GET['r'], SsoServer::$encryptionMethod, SsoServer::$paramPassphrase) . SsoServer::generateSecureParams());
        exit();
    }

    public static function checkAuthToken()
    {
        if (SsoServer::$authTokenValidity != null) {
            return SsoServer::$authTokenValidity;
        }

        if (!isset($_SESSION["authToken"]) || !isset($_SESSION["accountEmail"]) || $_SESSION["authToken"] == "" || $_SESSION["accountEmail"] == "") {
            return false;
        }

        $data = json_decode(SsoServer::getWebsiteData(SsoServer::$apiPath . "?mode=checkAuthToken&email=" . $_SESSION["accountEmail"] . "&authToken=" . $_SESSION["authToken"] . "&fingerprint="), true);

        $loggedIn = $data["success"];

        if (!$loggedIn) {
            $_SESSION["authToken"] = "";
            $_SESSION["accountEmail"] = "";
        }

        SsoServer::$authTokenValidity = $loggedIn;

        return $loggedIn;
    }

    public static function login($email, $password)
    {
        $data = json_decode(SsoServer::getWebsiteData(SsoServer::$apiPath . "?mode=login&email=$email&password=$password&fingerprint="), true);

        if ($data["success"] != true) {
            return false;
        }

        $_SESSION["accountEmail"] = $email;
        $_SESSION["authToken"] = $data["additionalInformation"]["authToken"];

        if (!SsoServer::checkAuthToken()) {
            $_SESSION["accountEmail"] = "";
            $_SESSION["authToken"] = "";
            return false;
        }
        return true;
    }

    public static function logout()
    {
        json_decode(SsoServer::getWebsiteData(SsoServer::$apiPath . "?mode=logout&email=" . $_SESSION["accountEmail"]), true);
        $_SESSION["authToken"] = "";
        SsoServer::$authTokenValidity = null;
    }

    private static function getWebsiteData($url)
    {
        $context = stream_context_create(array(
            'http' => array(
                'header' => 'Authorization: Basic ' . base64_encode("login:password")
            )
        ));
        return file_get_contents($url, false, $context);
    }
}

