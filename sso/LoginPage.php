<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>MariusFreitag SSO</title>
    <link rel="stylesheet" href="https://mariusfreitag.de/shared/styles_default.css"/>
    <link rel="icon" type="image/x-icon" href="https://mariusfreitag.de/shared/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<h1><em>MariusFreitag</em> SingleSignOn</h1>
<?php
if (isset($_GET['login_error'])) {
    echo "<p><em>Error logging in. Please try again.</em></p>";
}
?>
<form name="login" action="" method="post">
    <input type="hidden" name="r" value="<?php echo $_GET['r'] ?>">
    <label for="email">Email:</label>
    <input type="email"
           id="email"
           name="email" <?php echo isset($_SESSION["accountEmail"]) ? "value=\"" . $_SESSION["accountEmail"] . "\" " : ""; ?>
           required>
    <br><br>
    <label for="password">Password:</label>
    <input type="password" id="password" name="password" required>
    <br><br>
    <button type="submit">Login</button>
</form>
</body>
</html>