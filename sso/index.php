<?php
require_once '../Helper.php';
require_once "SsoServer.php";

startSession();

// Check if request is login POST
if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['r'])) {
    SsoServer::login($_POST['email'], md5($_POST['password']));

    if (!SsoServer::checkAuthToken()) {
        header("Location: ./?login_error&r=" . urlencode($_POST['r']));
    } else {
        header("Location: ./?r=" . urlencode($_POST['r']));
    }

} else if (isset($_GET['r'])) {
    if (isset($_GET['logout'])) {
        SsoServer::logout();
        SsoServer::redirectToOrigin();

    } else if (!SsoServer::checkAuthToken()) {
        require_once 'LoginPage.php';

    } else {
        SsoServer::redirectToOrigin();
    }

} else {
    exit("SSO Error");
}

function startSession()
{
    session_set_cookie_params(2592000, "/", "accounts.mariusfreitag.de");
    session_start();
}