<?php

require_once 'Account.php';
require_once 'Config.php';
require_once 'DBHelper.php';
require_once 'Output.php';
require_once 'Sessions.php';
require_once 'index.php';

function generateToken()
{
    return bin2hex(openssl_random_pseudo_bytes(16));
}

function generateProductKey()
{
    $key = strtoupper(bin2hex(openssl_random_pseudo_bytes(16)));
    $newKey = substr($key, 0, 5) . "-" . substr($key, 6, 5) . "-" . substr($key, 10, 5) . "-" . substr($key, 14, 5) . "-" . substr($key, 19, 5);
    return $newKey;
}

function sendMail($to, $from, $subject, $message)
{

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'From: ' . $from . '' . "\r\n" .
        'Reply-To: ' . $from . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    return mail($to, $subject, $message, $headers);
}

function isEmailAddress($input)
{
    return \filter_var($input, FILTER_VALIDATE_EMAIL);
}

